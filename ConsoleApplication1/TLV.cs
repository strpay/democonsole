﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo
{

    public class TLV
    {
        private string tag;
        private int len;
        private string hexlen;
        private string value;

        public string Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        public int Len
        {
            get { return len; }
            set { len = value; }
        }

        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public string Hexlen
        {
            get { return hexlen; }
            set { hexlen = value; }
        }
    }
}
