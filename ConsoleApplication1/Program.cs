﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;

namespace demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string a55="9F2608FAE6BDE3AD0AB9929F2701809F101307010103A02002010A010000000000EDB228969F3704E2BE38D89F360201C79505000004E0009A031608069C01009F02060000000000105F2A02015682027C009F1A0201569F03060000000000009F3303E0F9C89F34030203009F3501229F1E0838323039393931348408A0000003330101019F090200209F4104000012559F631003090000333530310000000000000000";
            //9505008004E8009F1E086D6639305F3031009F101307010103212002010A0100000000003298D2549F36020020
            a55 = "9505008004E8009F1E086D6639305F3031009F101307010103212002010A0100000000003298D2549F36020020";
           var list=Split8583A55(a55);
            foreach (var item in list)
            {
                Console.WriteLine(item.Tag + ":" + item.Hexlen+":"+item.Len+":"+item.Value);
            }
            Console.ReadLine();
        }

        public static List<TLV> Split8583A55(string tlvhex)
        {
            List<TLV>  list=new List<TLV>();
            string info = tlvhex;
            int index = 0;
            string tag = string.Empty;

            while (index < info.Length)
            {
                TLV m = new TLV();
                //==========TAG===========
                tag = info.Substring(index, 2);
                int tagbyte = GetTagByte(tag);
                m.Tag = tagbyte == 2 ? info.Substring(index, 4) : tag;
                index = index + m.Tag.Length;

                //=========Len================
                string hexlen = info.Substring(index, 2);
                int lenbytes = 0;
                bool issingle = GetLenByte(hexlen, out lenbytes);
                if (issingle)
                {
                    m.Hexlen = hexlen;
                    m.Len = lenbytes;
                }
                else
                {
                    m.Hexlen = hexlen;
                    hexlen = info.Substring(index + 2, lenbytes*2); //截取实际L长度范围
                    m.Hexlen = m.Hexlen + hexlen; //组合16进制长度

                    int nextlen = Convert.ToInt32(hexlen, 16); //长度hex转10进制
                    m.Len = nextlen;
                }

                index += m.Hexlen.Length;

                //===============Value========================
                m.Value = info.Substring(index, lenbytes*2);
                index = index + m.Value.Length; //索引变更到Value起始位置
                list.Add(m);
            }
            return list;

        }

        public static int GetTagByte(string hextag)
        {
            //获取TAG所占字节数
            // string tag1 = a55.Substring(0, 2);
            string tagbin = hex2binary(hextag);
            if (tagbin.Substring(tagbin.Length - 4, 4) == "1111")
            {
                return 2;
            }
            return 1;
        }

        /// <summary>
        /// 验证是否是单字节长度 如果是则直接返回后续长度 ，否则返回的则是L的后续字节数
        /// </summary>
        /// <param name="hexlen">L Hex格式</param>
        /// <param name="reallen">true 返回V字节数 false 返回L后续字节数</param>
        /// <returns></returns>
        public static bool GetLenByte(string hexlen, out int reallen)
        {
            bool isonlyone = true;
            string value = hex2binary(hexlen);
            int len = 0;
            if (value.Substring(0, 1) == "0")
            {
                len = Convert.ToInt32(value, 2); //转10进制
                reallen = len;
                return true;
            }
            //表示是多个字节
            string binarylen = value.Substring(1, 7); //获取后7位
            len = Convert.ToInt32(binarylen, 2); //转10进制
            reallen = len;
            return false;
        }


          
        public static string hex2binary(string hexstr)
        {
            int res = Convert.ToInt32(hexstr, 16);//hex转10进制
            string j = Convert.ToString(res, 2); //10进制转二进制
            return j.PadLeft(8, '0');
        }
    }
}
